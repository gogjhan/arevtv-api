import express from 'express'
import mongoose from 'mongoose'

/**
 * LOCAL MODULES
 */
import env from 'env'
import passport from 'modules/passport'
import initSockets from 'modules/sockets'
import transporter from 'modules/transporter'
import * as middleware from 'middleware'

/**
 * ROUTES
 */
import publicRoutes from 'routes/public'
import authRoutes from 'routes/auth'
import apiRoutes from 'routes/api'

const PORT = process.env.PORT || env.port
const app = express()

mongoose.connect(env.mongodb.url)

transporter.verify((err, success) => {
    if (err) {
        console.log(err)
        return
    }

    console.log('Server is ready to send messages...')
})

app.use(require('morgan')('dev'))
app.use(require('cookie-parser')())
app.use(require('body-parser').json())
app.use(require('body-parser').urlencoded({ extended: true }))
app.use(require('express-session')({ secret: 'gogjhan', resave: false, saveUninitialized: true }))
app.use(require('cors')({ origin: true, credentials: true }))
app.use(passport.initialize())
app.use(passport.session())

app.use('/storage', express.static('storage'))
app.use('/video', [middleware.ensureAuthenticated, express.static('video')])
app.use('/freevideo', express.static('freevideo'))

app.use('/', publicRoutes)
app.use('/auth', authRoutes)
app.use('/api', apiRoutes)

const http = app.listen(PORT, () => { console.log(`Server is listening on http://localhost:${PORT}`) })

global.io = initSockets(http)