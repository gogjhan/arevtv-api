import mongoose, { Schema } from 'mongoose'
import randomstring from 'randomstring'

const verificationCode = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    code: String,
    createdAt: { 
        type: Date, 
        default: Date.now 
    }
})

verificationCode.statics.protectedFields = ['_id', 'createdAt']

verificationCode.post('validate', (doc, next) => {
    if (doc.isNew) {
        doc.code = randomstring.generate(32)

        return next()
    } else {
        return next()
    }
})

export default mongoose.model('VerificationCode', verificationCode)
