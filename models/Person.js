import mongoose, { Schema } from 'mongoose'

const personSchema = new Schema({
    fullName: {
        en: {
            type: String,
            required: [true, '[en] Fullname field is required!']
        },
        ru: {
            type: String,
            required: [true, '[ru] Fullname field is required!']
        },
        hy: {
            type: String,
            required: [true, '[hy] Fullname field is required!']
        }
    },
    image: {
        type: String,
        required: [true, 'Image field is required!'],
        validate: {
            validator: (value) => {
                return /\.(gif|jpg|jpeg|tiff|png)$/i.test(value)
            },
            message: 'Invalid filetype'
        }
    },
    biography: {
        en: {
            type: String,
            default: ''
        },
        ru: {
            type: String,
            default: ''
        },
        hy: {
            type: String,
            default: ''
        }
    },
    type: {
        type: String,
        enum: ['director', 'actor']
    },
    createdAt: { 
        type: Date, 
        default: Date.now 
    }
})

personSchema.statics.protectedFields = ['_id', 'image', 'createdAt']
personSchema.statics.translatable = ['fullName', 'biography']

const Person = mongoose.model('Person', personSchema)

export default Person
