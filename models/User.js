import bcrypt from 'bcryptjs'
import { handleErrors } from 'modules'
import mongoose, { Schema } from 'mongoose'

const userSchema = new Schema({
    name: String,
    email: {
        type: String,
        validate: {
            validator: (value) => {
                return /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value)
            },
            message: '{VALUE} is not a valid email address!'
        }
    },
    username: {
        type: String
    },
    password: { 
        type: String,
        minlength: [6, 'Password must be at least 6 characters long']
    },
    image: {
        type: String,
        validate: {
            validator: (value) => {
                if (value.startsWith('https://graph.facebook.com')) {
                    return true
                }

                return /\.(gif|jpg|jpeg|tiff|png)$/i.test(value)
            },
            message: 'Invalid filetype'
        }
    },
    stripeId: String,
    idram: String,
    facebookId: String,
    vkId: String,
    instagramId: String,
    twitterId: String,
    socket: String,
    admin: Boolean,
    paypalPaymentDate: Date,
    idramPaymentDate: Date,
    verified: {
        type: Boolean,
        default: false
    },
    registeredAt: { 
        type: Date, 
        default: Date.now 
    }
})

userSchema.post('validate', function (doc, next) {
    if (this.isModified('password')) {
        doc.password = bcrypt.hashSync(doc.password, 10)
    }

    return next()
})

userSchema.statics.protectedFields = [
    '_id',
    '__v',
    'image',
    'admin',
    'password',
    'registeredAt',
    'stripeId',
    'facebookId',
    'vkId',
    'instagramId',
    'twitterId',
    'socket',
    'verified'
]

userSchema.statics.seed = async function () {
    try {
        const users = await this.find()

        if (!users.length) {
            let user = new this({
                email: 'admin@arev.tv',
                password: '123456',
                admin: true,
                verified: true
            })

            await user.save()
        }
    } catch (e) {
        handleErrors(e)
    }
}

const User = mongoose.model('User', userSchema)

User.seed()

export default User
