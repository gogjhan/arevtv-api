import mongoose, { Schema } from 'mongoose'

const slideSchema = new Schema({
    title: {
        en: {
            type: String,
            default: '',
            validate: {
                validator: (value) => value.length < 256,
                message: '[en] Title cannot be include more than 256 characters.'
            }
        },
        ru: {
            type: String,
            default: '',
            validate: {
                validator: (value) => value.length < 256,
                message: '[ru] Title cannot be include more than 256 characters.'
            }
        },
        hy: {
            type: String,
            default: '',
            validate: {
                validator: (value) => value.length < 256,
                message: '[hy] Title cannot be include more than 256 characters.'
            }
        }
    },
    content: {
        en: {
            type: String,
            default: ''
        },
        ru: {
            type: String,
            default: ''
        },
        hy: {
            type: String,
            default: ''
        }
    },
    action: String,
    cover: {
        type: String,
        required: [true, 'Cover field is required!'],
        validate: {
            validator: (value) => /\.(gif|jpg|jpeg|tiff|png)$/i.test(value),
            message: 'Invalid filetype'
        }
    },
    page: {
        type: String,
        required: [true, 'page field is required'],
        enum: ['home', 'movies', 'arevprojects', 'music', 'special-projects', 'trailers']
    },
    order: {
        type: Number,
        default: 0
    },
    createdAt: { 
        type: Date, 
        default: Date.now 
    }
})

slideSchema.statics.protectedFields = ['_id', 'createdAt']
slideSchema.statics.translatable = ['title', 'content']

const Slide = mongoose.model('Slide', slideSchema)

export default Slide
