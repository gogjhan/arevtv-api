import mongoose, { Schema } from 'mongoose'
import { handleErrors } from 'modules'

const adSchema = new Schema({
    placement: {
        type: String,
        required: [true, 'Placement field cannot be empty!'],
        unique: true
    },
    title: {
        en: {
            type: String,
            default: ''
        },
        ru: {
            type: String,
            default: ''
        },
        hy: {
            type: String,
            default: ''
        }
    },
    content: {
        en: {
            type: String,
            default: ''
        },
        ru: {
            type: String,
            default: ''
        },
        hy: {
            type: String,
            default: ''
        }
    },
    callback: String,
    image: {
        type: String,
        validate: {
            validator: (value) => {
                if (value && value.length) {
                    return /\.(gif|jpg|jpeg|tiff|png)$/i.test(value)
                }

                return true
            },
            message: 'Invalid filetype'
        }
    },
    createdAt: { 
        type: Date, 
        default: Date.now 
    }
})

adSchema.statics.protectedFields = ['_id', 'image', 'createdAt']
adSchema.statics.translatable = ['title', 'content']

adSchema.statics.seed = async function () {
    try {
        const ads = await this.find()

        if (!ads.length) {
            await this.insertMany([
                { placement: 'home-top' },
                { placement: 'home-middle' },
                { placement: 'home-bottom' },
                { placement: 'movies' },
                { placement: 'arevprojects' },
                { placement: 'music' },
                { placement: 'special-projects' },
                { placement: 'trailers' },
                { placement: 'project' }
            ])
        }
    } catch (e) {
        handleErrors(e)
    }
}

const Ad = mongoose.model('Ad', adSchema)

Ad.seed()

export default Ad
