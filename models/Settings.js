import mongoose, {
    Schema
} from 'mongoose'

import {
    handleErrors
} from 'modules'

const settingsSchema = new Schema({
    liveVideoCover: {
        type: String,
        required: [true, 'Cover is required!'],
        validate: {
            validator: (value) => {
                return /\.(gif|jpg|jpeg|tiff|png)$/i.test(value)
            },
            message: 'Invalid filetype'
        }
    },
    liveVideoURI: String,
    liveVideoTitle: {
        en: {
            type: String,
            default: ''
        },
        ru: {
            type: String,
            default: ''
        },
        hy: {
            type: String,
            default: ''
        }
    },
    liveVideoDescription: {
        en: {
            type: String,
            default: ''
        },
        ru: {
            type: String,
            default: ''
        },
        hy: {
            type: String,
            default: ''
        }
    }
})

settingsSchema.statics.protectedFields = ['_id']
settingsSchema.statics.translatable = ['liveVideoTitle', 'liveVideoDescription']

settingsSchema.statics.seed = async function () {
    try {
        const settings = await this.find()

        if (!settings.length) {
            await this.create({
                youtubeLive: ''
            })
        }
    } catch (e) {
        handleErrors(e)
    }
}

const Settings = mongoose.model('Setting', settingsSchema)

Settings.seed()

export default Settings
