import mongoose, { Schema } from 'mongoose'

const pageSchema = new Schema({
    slug: {
        type: String,
        required: [true, 'Slug field cannot be empty!'],
        unique: true
    },
    title: {
        en: {
            type: String,
            default: ''
        },
        ru: {
            type: String,
            default: ''
        },
        hy: {
            type: String,
            default: ''
        }
    },
    content: {
        en: {
            type: String,
            default: ''
        },
        ru: {
            type: String,
            default: ''
        },
        hy: {
            type: String,
            default: ''
        }
    },
    image: {
        type: String,
        validate: {
            validator: (value) => {
                return /\.(gif|jpg|jpeg|tiff|png)$/i.test(value)
            },
            message: 'Invalid filetype'
        }
    },
    createdAt: { 
        type: Date, 
        default: Date.now 
    }
})

pageSchema.statics.protectedFields = ['_id', 'image', 'createdAt']
pageSchema.statics.translatable = ['title', 'content']

pageSchema.statics.seed = async function () {
    try {
        const pages = await this.find()

        if (!pages.length) {
            let newPages = [
                { slug: 'about-us' },
                { slug: 'terms-and-conditions' }
            ]
            
            let newPage = {}
            let createNewPages = []
            newPages.forEach(ad => {
                newPage = new this(ad)
                createNewPages.push(newPage.save())
            })
            
            await Promise.all(createNewPages)
        }
    } catch (e) {
        console.log(e)
    }
}

const Page = mongoose.model('Page', pageSchema)

Page.seed()

export default Page
