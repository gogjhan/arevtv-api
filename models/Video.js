import mongoose, { Schema } from 'mongoose'

const videoSchema = new Schema({
    slug: {
        en: {
            type: String,
            required: [true, '[en] Slug is required!']
        },
        ru: {
            type: String,
            required: [true, '[ru] Slug is required!']
        },
        hy: {
            type: String,
            required: [true, '[hy] Slug is required!']
        }
    },
    metaDescription: {
        en: {
            type: String,
            default: ''
        },
        ru: {
            type: String,
            default: ''
        },
        hy: {
            type: String,
            default: ''
        }
    },
    metaKeywords: {
        en: {
            type: String,
            default: ''
        },
        ru: {
            type: String,
            default: ''
        },
        hy: {
            type: String,
            default: ''
        }
    },
    headTitle: {
        en: {
            type: String,
            default: ''
        },
        ru: {
            type: String,
            default: ''
        },
        hy: {
            type: String,
            default: ''
        }
    },
    title: {
        en: {
            type: String,
            required: [true, '[en] Title is required!']
        },
        ru: {
            type: String,
            required: [true, '[ru] Title is required!']
        },
        hy: {
            type: String,
            required: [true, '[hy] Title is required!']
        }
    },
    summary: {
        en: {
            type: String,
            default: ''
        },
        ru: {
            type: String,
            default: ''
        },
        hy: {
            type: String,
            default: ''
        }
    },
    url: String,
    cover: {
        type: String,
        required: [true, 'Cover is required!'],
        validate: {
            validator: (value) => {
                return /\.(gif|jpg|jpeg|tiff|png)$/i.test(value)
            },
            message: 'Invalid filetype'
        }
    },
    type: {
        type: String,
        enum: ['trailer', 'episode'],
        default: 'trailer'
    },
    order: {
        type: Number,
        default: 0
    },
    project: {
        type: Schema.Types.ObjectId,
        ref: 'Project'
    },
    premium: {
        type: Boolean,
        default: false
    },
    votes: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    createdAt: { 
        type: Date, 
        default: Date.now 
    }
})

videoSchema.statics.protectedFields = ['_id', '__v', 'cover', 'createdAt']
videoSchema.statics.translatable = ['headTitle', 'title', 'summary', 'metaDescription', 'metaKeywords', 'slug']

const Video = mongoose.model('Video', videoSchema)

export default Video
