import mongoose, { Schema } from 'mongoose'

const projectSchema = new Schema({
    slug: {
        en: {
            type: String,
            required: [true, '[en] Slug is required!']
        },
        ru: {
            type: String,
            default: '',
            required: [true, '[ru] Slug is required!']
        },
        hy: {
            type: String,
            default: '',
            required: [true, '[hy] Slug is required!']
        }
    },
    metaDescription: {
        en: {
            type: String,
            default: ''
        },
        ru: {
            type: String,
            default: ''
        },
        hy: {
            type: String,
            default: ''
        }
    },
    metaKeywords: {
        en: {
            type: String,
            default: ''
        },
        ru: {
            type: String,
            default: ''
        },
        hy: {
            type: String,
            default: ''
        }
    },
    headTitle: {
        en: {
            type: String,
            default: ''
        },
        ru: {
            type: String,
            default: ''
        },
        hy: {
            type: String,
            default: ''
        }
    },
    title: {
        en: {
            type: String,
            required: [true, '[en] Title is required!']
        },
        ru: {
            type: String,
            required: [true, '[ru] Title is required!']
        },
        hy: {
            type: String,
            required: [true, '[hy] Title is required!']
        }
    },
    summary: {
        en: {
            type: String,
            default: ''
        },
        ru: {
            type: String,
            default: ''
        },
        hy: {
            type: String,
            default: ''
        }
    },
    poster: {
        type: String,
        required: [true, 'Poster is required!'],
        validate: {
            validator: (value) => {
                return /\.(gif|jpg|jpeg|tiff|png)$/i.test(value)
            },
            message: 'Invalid filetype'
        }
    },
    cover: {
        type: String,
        validate: {
            validator: (value) => {
                return /\.(gif|jpg|jpeg|tiff|png)$/i.test(value)
            },
            message: 'Invalid filetype'
        }
    },
    banner: {
        type: String,
        validate: {
            validator: (value) => {
                return /\.(gif|jpg|jpeg|tiff|png)$/i.test(value)
            },
            message: 'Invalid filetype'
        }
    },
    bannerUrl: String,
    director: {
        type: Schema.Types.ObjectId,
        ref: 'Person'
    },
    actors: [{
        type: Schema.Types.ObjectId,
        ref: 'Person'
    }],
    genres: [String],
    duration: Number,
    voting: {
        type: Boolean,
        default: false
    },
    published: Date,
    type: {
        type: String,
        enum: ['movies', 'arevprojects', 'music', 'special-projects']
    },
    seasons: [{
        reference: {
            type: Schema.Types.ObjectId,
            ref: 'Project'
        },
        name: String
    }],
    popular: {
        type: Boolean,
        default: false
    },
    trailers: [{
        type: Schema.Types.ObjectId,
        ref: 'Video'
    }],
    episodes: [{
        type: Schema.Types.ObjectId,
        ref: 'Video'
    }],
    url: String,
    premium: {
        type: Boolean,
        default: true
    },
    recommendations: [{
        type: Schema.Types.ObjectId,
        ref: 'Movie'
    }],
    html: String,
    createdAt: { 
        type: Date, 
        default: Date.now 
    }
})

projectSchema.statics.protectedFields = ['_id', '__v', 'poster', 'cover', 'banner', 'createdAt']
projectSchema.statics.translatable = ['headTitle', 'title', 'summary', 'metaDescription', 'metaKeywords', 'slug']

const Project = mongoose.model('Project', projectSchema) 

export default Project
