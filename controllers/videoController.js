import { assign, unlink, handleErrors } from 'modules'
import { ERROR } from 'enum'
import { Video, Project } from 'models'

export async function all (req, res) {
    try {
        if (req.query.type && req.query.type.length) {
            const videos = await Video.find({
                type: req.query.type
            }).sort('-createdAt')

            return res.json({ videos })
        } else if (req.query.projectType && req.query.projectType.length) {
            const videos = await Video.aggregate([
                { $sort: { createdAt: -1 } },
                { $lookup: { from: 'projects', localField: 'project', foreignField: '_id', as: 'project' } },
                { $unwind: '$project' },
                {
                    $match: {
                        _id: { $exists: true },
                        type: 'trailer',
                        'project._id': { $exists: true },
                        'project.type': req.query.projectType
                    }
                }
            ])

            return res.json({ videos })
        }

        const videos = await Video.find({}).sort('-createdAt')

        return res.json({ videos })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function get (req, res) {
    try {
        const video = await Video.findById(req.params.id).populate(['project'])

        if (!video) {
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Video not found!' })
        }

        return res.json({
            video
        })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function getViaSlug (req, res) {
    try {
        const video = await Video.findOne({ [`slug.${req.params.locale}`]: req.params.slug }).populate(['project'])

        if (!video) {
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Video not found!' })
        }

        const episodes = await Video.find({
            project: video.project._id,
            _id: {
                $ne: video._id
            }
        })

        return res.json({
            video: {
                ...video._doc,
                episodes
            }
        })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function add (req, res) {
    try {
        let video = new Video()

        await assign(video, req, Video)

        return res.json({ video })
    } catch (e) {
        if (req.files) {
            Object.entries(req.files).forEach(([key, value]) => { unlink(value[0].path) })
        }

        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function update (req, res) {
    try {
        let video = await Video.findById(req.params.id)

        if (!video) {
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Video not found!' })
        }

        await assign(video, req, Video)

        return res.json({ video })
    } catch (e) {
        if (req.files) {
            Object.entries(req.files).forEach(([key, value]) => { unlink(value[0].path) })
        }

        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function vote (req, res) {
    try {
        let video = await Video.findOne({
            _id: req.params.id,
            votes: {
                $nin: [req.user._id]
            }
        })

        if (!video) {
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Video not found!' })
        }

        video.votes.push(req.user._id)

        await video.save()

        return res.json({
            success: true
        })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function remove (req, res) {
    try {
        const video = await Video.findById(req.params.id)

        if (!video) {
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Video not found!' })
        }

        // remove video from project
        await Project.update({
            trailers: { $in: [video._id] },
            episodes: { $in: [video._id] }
        }, {
            $pull: {
                trailers: video._id,
                episodes: video._id
            }
        })

        await unlink(video.cover)
        await video.remove()

        return res.json({ success: true })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}