import { Person, Movie } from 'models'
import { assign, unlink, handleErrors } from 'modules'
import { ERROR } from 'enum'

export async function all (req, res) {
    try {
        let query = {}
        if (req.query.filters) {
            let filters = JSON.parse(req.query.filters)
            Object.entries(filters).forEach(([key, filter]) => {
                if (filter) {
                    query = { [key]: { $regex: new RegExp(filter, 'i') }, ...query }
                }
            })
        }
        const people = await Person.find(query).sort('-createdAt')
        
        return res.json({ people })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function get (req, res) {
    try {
        const person = await Person.findById(req.params.id)

        if (!person)
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Invalid personId' })

        const [ actedMovies, directedMovies ] = await Promise.all([
            Movie.find({ actors: { $in: [person._id] } }),
            Movie.find({ directors: { $in: [person._id] } })
        ])

        return res.json({
            person: {
                ...person._doc,
                actedMovies,
                directedMovies
            }
        })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function add (req, res) {
    try {
        const person = new Person()
        await assign(person, req, Person)

        return res.json({ person })
    } catch (e) {
        if (req.files)
            Object.entries(req.files).forEach(([key, value]) => { unlink(value[0].path) })

        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function update (req, res) {
    try {
        let person = await Person.findById(req.params.id)

        if (!person)
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Invalid personId' })

        await assign(person, req, Person)

        return res.json({ person })
    } catch (e) {
        if (req.files)
            Object.entries(req.files).forEach(([key, value]) => { unlink(value[0].path) })

        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function remove (req, res) {
    try {
        const person = await Person.findById(req.params.id)
        
        if (!person)
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Invalid movieId' })

        await unlink(person.image)
        await person.remove()

        return res.json({ success: true })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}
