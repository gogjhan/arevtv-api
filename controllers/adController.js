import { handleErrors, assign, unlink } from 'modules'
import { Ad } from 'models'
import { ERROR } from 'enum'

export async function all (req, res) {
    try {
        const ads = await Ad.find({}).sort('placement')

        return res.json({ ads })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function get (req, res) {
    try {
        const ad = await Ad.findOne({ placement: req.params.placement })

        if (!ad) {
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Ad not found!' })
        }

        return res.json({ ad })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function add (req, res) {
    try {
        const ad = new Ad()
        
        await assign(ad, req, Ad)

        return res.json({ ad })
    } catch (e) {
        if (req.files) {
            Object.entries(req.files).forEach(([key, value]) => { unlink(value[0].path) })
        }

        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function update (req, res) {
    try {
        let ad = await Ad.findById(req.params.id)

        if (!ad) {
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Ad not found!' })
        }

        await assign(ad, req, Ad)

        if (req.body.image === 'null') {
            ad.image = null
        }

        await ad.save()

        return res.json({ ad })
    } catch (e) {
        if (req.files) {
            Object.entries(req.files).forEach(([key, value]) => { unlink(value[0].path) })
        }

        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function remove (req, res) {
    try {
        const ad = await Ad.findById(req.params.id)

        if (!ad) {
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Ad not found!' })
        }

        await unlink(ad.image)
        await ad.remove()
        
        return res.json({ success: true })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}