import * as authController from './authController'
import * as projectController from './projectController'
import * as videoController from './videoController'
import * as slideController from './slideController'
import * as userController from './userController'
import * as personController from './personController'
import * as pageController from './pageController'
import * as adController from './adController'
import * as paymentController from './paymentController'
import * as settingsController from './settingsController'

export {
    authController,
    projectController,
    videoController,
    slideController,
    userController,
    personController,
    pageController,
    adController,
    paymentController,
    settingsController
}