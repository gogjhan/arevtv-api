import { Settings } from 'models'
import {
    assign,
    handleErrors,
    unlink
} from 'modules'

export async function all (req, res) {
    try {
        const settings = await Settings.findOne()

        return res.json({
            settings
        })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function update (req, res) {
    try {
        let settings = await Settings.findOne()

        await assign(settings, req, Settings)

        await settings.save()

        return res.json({
            settings
        })
    } catch (e) {
        if (req.files) {
            Object.entries(req.files).forEach(([key, value]) => { unlink(value[0].path) })
        }

        handleErrors(e)
        return res.status(500).send(e)
    }
}