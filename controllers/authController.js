/**
 * local modules
 */
import { ERROR } from 'enum'
import env from 'env'
import { User, VerificationCode } from 'models'
import { assign, handleErrors } from 'modules'
import transporter from 'modules/transporter'
import stripe from 'modules/stripe'

export async function get (req, res) {
    try {
        let customer = {}
        const user = await User.findById(req.user._id)

        if (user.stripeId && user.stripeId.length) {
            customer = await stripe.customers.retrieve(user.stripeId)
            
            if (customer.deleted) {
                customer = await stripe.customers.create({
                    description: `Customer for ${user._id}`,
                    email: user.username
                })

                user.stripeId = customer.id
                await user.save()
            }
        }

        user._doc.stripe = customer
        return res.json({ user })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function update (req, res) {
    try {
        const user = await User.findById(req.user._id)

        if (req.body.newPassword && req.body.newPassword.length) {
            user.password = req.body.newPassword
        }

        await assign(user, req, User)

        return res.json({ user })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function register (req, res) {
    try {
        let user = new User()

        if (!req.body.username || !req.body.username.length) return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Username field is required!' })
        if (!req.body.password || !req.body.password.length) return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Password field is required!' })
        if (!req.body.passwordConfirmation || req.body.password !== req.body.passwordConfirmation) return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Invalid password confirmation!' })

        const duplicateUser = await User.findOne({ username: req.body.username })

        if (duplicateUser) {
            return res.status(ERROR.INVALID_DATA).json({
                status: ERROR.INVALID_DATA,
                message: 'Duplicate username!'
            })
        }

        user.password = req.body.password
        await assign(user, req, User)

        let [customer] = await Promise.all([
            await stripe.customers.create({
                description: `Customer for ${user._id}`,
                email: user.username
            })
        ])

        user.stripeId = customer.id
        await user.save()

        return res.json({ success: true })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function verify (req, res, next) {
    try {
        const verification = await VerificationCode.findOne({ code: req.body.code })

        if (!verification) {
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Invalid verification code!' })
        }

        const user = await User.findById(verification.user)

        if (!user) {
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Invalid verification code!' })
        }

        user.verified = true
        await Promise.all([
            user.save(),
            verification.remove()
        ])

        return res.json({ success: true })
    } catch (e) {
        handleErrors(e)
        return next(e)
    }
}

export async function login (req, res, next) {
    try {
        req.session.save()

        return res.json({ success: true })
    } catch (e) {
        handleErrors(e)
        return next(e)
    }
}

export async function logout (req, res, next) {
    try {
        req.logout()
        req.session.save()

        return res.json({ success: true })
    } catch (e) {
        handleErrors(e)
        return next(e)
    }
}

export async function resetPassword (req, res) {
    try {
        const user = await User.findOne({ email: req.body.email })

        if (!user) {
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'User not found!' })
        }

        await VerificationCode.remove({ user: user._id })
        const reset = new VerificationCode({ user: user._id })

        await reset.save()

        let text = `Please follow this link to reset password ${env.appUrl}/en/callback/forgot-password?code=${reset.code}`

        await transporter.sendMail({
            from: env.nodemailer.fromAddress,
            to: user.email,
            subject: 'ArevTV password request',
            text
        })

        return res.json({ success: true })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function newPassword (req, res) {
    try {
        const verification = await VerificationCode.findOne({ code: req.body.code })

        if (!verification) {
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Invalid verification code!' })
        }

        if (!req.body.password) {
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'New password is required!' })
        }
        
        if (!req.body.passwordConfirmation || req.body.passwordConfirmation !== req.body.password) {
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Invalid password confirmation!' })
        }

        const user = await User.findById(verification.user)

        user.password = req.body.password

        await Promise.all([
            user.save(),
            verification.remove()
        ])

        return res.json({ success: true })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}
