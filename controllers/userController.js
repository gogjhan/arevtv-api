import { User } from 'models'
import { handleErrors, assign } from 'modules'
import { ERROR } from 'enum'

export async function all (req, res) {
    try {
        let users = await User.find({})

        return res.json({ users })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function get (req, res) {
    try {
        let user = await User.findById(req.params.id)

        return res.json({ user })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function update (req, res) {
    try {
        let user = await User.findById(req.params.id)

        if (!user) {
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'User not found!' })
        }

        if (req.body.newPassword && req.body.newPassword.length) {
            user.password = req.body.newPassword
        }

        await assign(user, req, User)

        return res.json({ user })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function seedUsername (req, res) {
    try {
        const users = await User.find()

        users.forEach(user => {
            user.username = user.email
        })

        await Promise.all(users.map(user => user.save()))

        return res.json({ success: true })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function remove (req, res) {
    try {
        let user = await User.findById(req.params.id)

        if (!user) {
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'User not found!' })
        }
        
        await user.remove()

        return res.json({ success: true })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}
