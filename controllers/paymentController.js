import axios from 'axios'
import stripe from 'modules/stripe'
import User from 'models/User'
import urlencode from 'urlencode'
import md5 from 'blueimp-md5'
import env from 'env'

import { handleErrors } from 'modules'

export async function viaInecopay (req, res) {
    try {
        let response = await axios.post('https://inecopay.ru/inecogate/rest/api/v1.0/gate/register?partnerId=1427&paymentType=14&amount=1&api_key=FrMvDExuEHwyMFDHwGDoxErEMzMuzH0Mvoysnu2oDMsnnIvxps')

        console.log(response)

        return res.json({ response })
    } catch (e) {
        handleErrors(e)
        return res.status(500)
    }
}

export async function addCard (req, res) {
    try {
        if (!req.user.stripeId) {
            return res.status(500).json({ message: 'StripeID is required!' })
        }

        const cards = await stripe.customers.listCards(req.user.stripeId)
        for (let card of cards.data) {
            await stripe.customers.deleteCard(req.user.stripeId, card.id)
        }

        await stripe.customers.createSource(req.user.stripeId, { source: req.body.token })

        return res.json({ success: true })
    } catch (e) {
        handleErrors(e)
        return res.status(500)
    }
}

export async function unsubscribeStripe (req, res) {
    try {
        const customer = await stripe.customers.retrieve(req.user.stripeId)

        let list = []
        customer.subscriptions.data.forEach(subscription => {
            list.push(() => stripe.subscriptions.del(subscription.id))
        })

        await Promise.all(list.map(subscription => subscription()))

        return res.json({ success: true })
    } catch (e) {
        handleErrors(e)
        return res.status(500)
    }
}

export async function viaStripe (req, res) {
    try {
        await stripe.subscriptions.create({
            customer: req.user.stripeId,
            items: [{ plan: 'monthly' }]
        })

        return res.json({ success: true })
    } catch (e) {
        handleErrors(e)
        return res.status(500)
    }
}

export async function viaPaypal (req, res) {
    try {
        let url = `https://www.paypal.com/cgi-bin/webscr?`
        Object.entries(req.body).forEach(([key, value]) => {
            url += `${key}=${urlencode(value)}&`
        })
        url += 'cmd=_notify-validate'

        let response = await axios.post(url)
        let success = response.data === 'VERIFIED'

        const user = await User.findById(req.body.custom)
        global.io.to(user.socket).emit('paypal success', { success })

        if (success) {
            user.paypalPaymentDate = new Date()

            await user.save()
        }

        return res.json({ success })
    } catch (e) {
        handleErrors(e)
        return res.status(500)
    }
}

export async function viaIdram (req, res) {
    try {
        console.log(req.body)
        
        const user = await User.findById(req.body['USER_ID'])

        if (!user) {
            console.log('user not found')
            return res.status(500).json({ message: 'Invalid user id!' })
        }

        if (req.body['EDP_BILL_NO'] && 
            req.body['EDP_REC_ACCOUNT'] && 
            req.body['EDP_AMOUNT'] && 
            req.body['EDP_PRECHECK'] && 
            req.body['EDP_PRECHECK'] === 'YES') {
                if (req.body['EDP_AMOUNT'] !== '1700.00') {
                    console.log('invalid amount')
                    return res.status(500).json({ message: 'Invalid amount!' })
                }
                console.log('precheck OK')
                return res.send('OK')
        }

        if (req.body['EDP_PAYER_ACCOUNT'] &&
            req.body['EDP_BILL_NO'] &&
            req.body['EDP_REC_ACCOUNT'] &&
            req.body['EDP_AMOUNT'] &&
            req.body['EDP_TRANS_ID'] &&
            req.body['EDP_CHECKSUM']) {
                console.log('payment confirmed')

                if (req.body['EDP_AMOUNT'] !== '1700.00') {
                    console.log('invalid amount')
                    return res.status(500).json({ message: 'Invalid amount!' })
                }

                let hash = `${env.idram.clientId}:${req.body['EDP_AMOUNT']}:${env.idram.clientSecret}:${req.body['EDP_BILL_NO']}:${req.body['EDP_PAYER_ACCOUNT']}:${req.body['EDP_TRANS_ID']}:${req.body['EDP_TRANS_DATE']}`
                hash = md5(hash)
                console.log(hash.toUpperCase())
                console.log(req.body['EDP_CHECKSUM'].toUpperCase())
                if (hash.toUpperCase() !== req.body['EDP_CHECKSUM'].toUpperCase()) {
                    console.log('invalid hash')
                    return res.status(500).json({ message: 'Invalid data' })
                }

                user.idram = req.body['EDP_PAYER_ACCOUNT']
                user.idramPaymentDate = new Date()

                await user.save()

                console.log('payment successfull!')

                return res.send('OK')
        }

        console.log('something went wrong')
        return res.status(500).send('ERROR')
    } catch (e) {
        handleErrors(e)
        return res.status(500)
    }
}