import { assign, unlink, handleErrors } from 'modules'
import { ERROR } from 'enum'
import { Project, Video } from 'models'
import mongoose from 'mongoose'
import _ from 'lodash'

export async function all (req, res) {
    try {
        let query = {}
        if (req.query.filters) {
            let filters = JSON.parse(req.query.filters)
            
            if (filters.type) {
                if (['music', 'arevprojects', 'movies', 'special-projects'].includes(filters.type)) {
                    const projects = await Project.aggregate([
                        { $match: { type: filters.type } },
                        { $sort: { createdAt: -1 } },
                        {
                            $facet: {
                                popular: [
                                    { $match: { popular: true } },
                                    { $limit: 4 }
                                ],
                                newReleases: [
                                    { $match: { _id: { $exists: true } } },
                                    { $limit: 4 }
                                ],
                                all: [
                                    { $match: { _id: { $exists: true } } }
                                ]
                            }
                        }
                    ])

                    const other = await Project.find({
                        type: filters.type,
                        _id: {
                            $nin: [
                                ...projects[0].popular.map(project => project._id),
                                ...projects[0].newReleases.map(project => project._id)
                            ]
                        }
                    }).sort('-createdAt')

                    return res.json({
                        projects: {
                            ...projects[0],
                            other
                        }
                    })
                } else if (['trailers'].includes(filters.type)) {
                    const projects = await Video.aggregate([
                        { $sort: { createdAt: -1 } },
                        { $lookup: { from: 'projects', localField: 'project', foreignField: '_id', as: 'project' } },
                        { $unwind: '$project' },
                        {
                            $match: {
                                _id: { $exists: true },
                                type: 'trailer',
                                'project._id': { $exists: true }
                            }
                        },
                        {
                            $facet: {
                                movies: [
                                    { $match: { 'project.type': 'movies' } },
                                    { $limit: 4 }
                                ],
                                arevprojects: [
                                    { $match: { 'project.type': 'arevprojects' } },
                                    { $limit: 4 }
                                ],
                                music: [
                                    { $match: { 'project.type': 'music' } },
                                    { $limit: 4 }
                                ],
                                specialProjects: [
                                    { $match: { 'project.type': 'special-projects' } },
                                    { $limit: 4 }
                                ]
                            }
                        }
                    ])

                    return res.json({ projects: projects[0] })
                } else if (['home'].includes(filters.type)) {
                    const projects = await Project.aggregate([
                        { $sort: { createdAt: -1 } },
                        {
                            $facet: {
                                all: [
                                    { $match: { _id: { $exists: true } } },
                                    { $limit: 4 }
                                ],
                                newReleases: [
                                    { $match: { _id: { $exists: true } } },
                                    { $limit: 4 }
                                ],
                                popular: [
                                    { $match: { popular: true } },
                                    { $limit: 4 }
                                ],
                                movies: [
                                    { $match: { type: 'movies' } },
                                    { $limit: 4 }
                                ],
                                music: [
                                    { $match: { type: 'music' } },
                                    { $limit: 4 }
                                ],
                                specialProjects: [
                                    { $match: { type: 'special-projects' } },
                                    { $limit: 4 } 
                                ]
                            }
                        }
                    ])

                    const arevprojects = await Project.aggregate([
                        { $match: { type: 'arevprojects' } },
                        { $lookup: { from: 'videos', localField: 'episodes', foreignField: '_id', as: 'episodes' } },
                        { $sort: { 'episodes.createdAt': -1 } },
                        { $limit: 4 }
                    ])

                    return res.json({
                        projects: {
                            ...projects[0],
                            arevprojects
                        }
                    })
                }
            }
        }

        const projects = await Project.find(query).sort('-createdAt')

        return res.json({ projects: { all: projects } })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function get (req, res) {
    try {
        const project = await Project
                                     .findById(req.params.id)
                                     .populate(['director', 'actors', 'seasons.reference'])
                                     .populate({
                                         path: 'episodes',
                                         options: {
                                             sort: { 'order': 1 }
                                         }
                                     })
                                     .populate({
                                        path: 'trailers',
                                        options: {
                                            sort: { 'order': 1 }
                                        }
                                    })

        if (!project)
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Invalid projectId' })

        return res.json({ project })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function getViaSlug (req, res) {
    try {
        const project = await Project
                                     .findOne({ [`slug.${req.params.locale}`]: req.params.slug })
                                     .populate(['director', 'actors', 'seasons.reference'])
                                     .populate({
                                         path: 'episodes',
                                         options: {
                                             sort: { 'order': 1 }
                                         }
                                     })
                                     .populate({
                                        path: 'trailers',
                                        options: {
                                            sort: { 'order': 1 }
                                        }
                                    })

        if (!project)
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Invalid projectId' })

        return res.json({ project })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function add (req, res) {
    try {
        let project = new Project()

        await assign(project, req, Project)

        return res.json({ project })
    } catch (e) {
        if (req.files) {
            Object.entries(req.files).forEach(([key, value]) => { unlink(value[0].path) })
        }

        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function update (req, res) {
    try {
        let project = await Project.findById(req.params.id)

        if (!project) {
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Invalid projectId' })
        }

        if (!req.body.actors) project.actors = []
        if (!req.body.trailers) project.trailers = []
        if (!req.body.genres) project.genres = []
        if (!req.body.episodes) project.episodes = []
        if (!req.body.episodes) project.seasons = []

        await Promise.all([
            ...project.trailers.map(trailer => Video.findByIdAndUpdate(trailer, { $unset: { project: '' } })),
            ...project.episodes.map(episode => Video.findByIdAndUpdate(episode, { $unset: { project: '' } }))
        ])

        await assign(project, req, Project)

        await Promise.all([
            ...project.trailers.map((trailer, index) => Video.findByIdAndUpdate(trailer, { $set: { project: project._id, order: index } })),
            ...project.episodes.map((episode, index) => Video.findByIdAndUpdate(episode, { $set: { project: project._id, order: index } }))
        ])

        return res.json({ project })
    } catch (e) {
        if (req.files) {
            Object.entries(req.files).forEach(([key, value]) => { unlink(value[0].path) })
        }

        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function remove (req, res) {
    try {
        const project = await Project.findById(req.params.id)

        if (!project) {
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Invalid projectId' })
        }

        await Promise.all([
            unlink(project.poster),
            unlink(project.cover)
        ])

        await project.remove()

        return res.json({ success: true })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}