import { Slide } from 'models'
import { assign, unlink, handleErrors } from 'modules'
import { ERROR } from 'enum'

export async function all (req, res) {
    try {
        let query = {}
        let search = {}

        if (req.query.search) {
            try {
                search = JSON.parse(req.query.search)
                Object.entries(search).forEach(([key, value]) => {
                    query = { [key]: value, ...query }
                })
            } catch (e) {
                console.log('Invalid JSON in search query!')
            }
        }

        const slides = await Slide.find(query).sort('order')
        
        return res.json({ slides })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function add (req, res) {
    try {
        let slide = new Slide()
        await assign(slide, req, Slide)

        return res.json({ slide })
    } catch (e) {
        if (req.files) {
            Object.entries(req.files).forEach(([key, value]) => { unlink(value[0].path) })
        }

        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function update (req, res) {
    try {
        let slide = await Slide.findById(req.params.id)

        if (!slide)
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Invalid slideId' })

        await assign(slide, req, Slide)

        return res.json({ slide })
    } catch (e) {
        if (req.files) {
            Object.entries(req.files).forEach(([key, value]) => { unlink(value[0].path) })
        }

        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function remove (req, res) {
    try {
        const slide = await Slide.findById(req.params.id)

        if (!slide)
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Invalid slideId' })

        await unlink(slide.cover)
        await slide.remove()
        
        return res.json({ success: true })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}