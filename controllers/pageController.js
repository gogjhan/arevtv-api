import { handleErrors, assign, unlink } from 'modules'
import { Page } from 'models'
import { ERROR } from 'enum'

export async function all (req, res) {
    try {
        const pages = await Page.find()

        return res.json({ pages })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function get (req, res) {
    try {
        const page = await Page.findOne({ slug: req.params.slug })

        if (!page) {
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Page not found!' })
        }

        return res.json({ page })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function add (req, res) {
    try {
        const page = new Page()
        
        await assign(page, req, Page)

        return res.json({ page })
    } catch (e) {
        if (req.files) {
            Object.entries(req.files).forEach(([key, value]) => { unlink(value[0].path) })
        }

        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function update (req, res) {
    try {
        let page = await Page.findById(req.params.id)

        if (!page) {
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Page not found!' })
        }

        await assign(page, req, Page)

        return res.json({ page })
    } catch (e) {
        if (req.files) {
            Object.entries(req.files).forEach(([key, value]) => { unlink(value[0].path) })
        }

        handleErrors(e)
        return res.status(500).send(e)
    }
}

export async function remove (req, res) {
    try {
        const page = await Page.findById(req.params.id)

        if (!page) {
            return res.status(ERROR.INVALID_DATA).json({ status: ERROR.INVALID_DATA, message: 'Page not found!' })
        }

        await unlink(page.image)
        await page.remove()
        
        return res.json({ success: true })
    } catch (e) {
        handleErrors(e)
        return res.status(500).send(e)
    }
}