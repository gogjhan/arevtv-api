import Stripe from 'stripe'
import env from 'env'

export default new Stripe(env.stripeKey)