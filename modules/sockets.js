import socketIO from 'socket.io'
import User from 'models/User'

export default function (http) {
    const io = socketIO(http, {
        serveClient: false
    })

    io.on('connection', async (socket) => {
        await User.findByIdAndUpdate(socket.handshake.query.userId, { $set: { socket: socket.id } })

        socket.on('disconnect', async (socket) => {
            await User.findByIdAndUpdate(socket.handshake.query.userId, { $unset: { socket: 1 } })
        })
    })

    return io
}
