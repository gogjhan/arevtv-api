import bcrypt from 'bcryptjs'
import passport from 'passport'
import { Strategy as LocalStrategy } from 'passport-local'
import { Strategy as FacebookStrategy } from 'passport-facebook'
import { Strategy as VKontakteStrategy } from 'passport-vkontakte'
import { Strategy as TwitterStrategy } from 'passport-twitter'

import env from 'env'
import { User } from 'models'
import stripe from 'modules/stripe'

passport.use(new LocalStrategy({
    usernameField: 'username'
}, async (username, password, done) => {
    try {
        const user = await User.findOne({ username })

        if (!user) {
            return done(null, false)
        }

        if (!bcrypt.compareSync(password, user.password)) {
            return done(null, false)
        }

        if (!user.stripeId) {
            const customer = await stripe.customers.create({
                description: `Customer for ${user._id}`,
                email: user.username
            })
    
            user.stripeId = customer.id

            await user.save()
        }

        return done(null, user)
    } catch (e) {
        return done(e)
    }
}))

/**
 * facebook auth
 */
passport.use(new FacebookStrategy({
    clientID: env.facebook.clientId,
    clientSecret: env.facebook.clientSecret,
    callbackURL: `${env.appUrl}/`
}, async (token, refreshToken, profile, done) => {
    try {
        let user = await User.findOne({ 'facebookId': profile.id })

        // create new user if not found
        if (!user) {
            user = new User({
                facebookId: profile.id,
                name: profile.displayName
            })

            await user.save()
        }

        if (!user.stripeId) {
            const customer = await stripe.customers.create({
                description: `Customer for ${user._id}`,
                username: user.username
            })
    
            user.stripeId = customer.id

            await user.save()
        }

        return done(null, user)
    } catch (e) {
        return done(e)
    }
}))

/**
 * vkontakte auth
 */
passport.use(new VKontakteStrategy({
    clientID: env.vk.clientId,
    clientSecret: env.vk.clientSecret,
    callbackURL: `${env.appUrl}/vk`
}, async (token, refreshToken, profile, done) => {
    try {
        let user = await User.findOne({ 'vkId': profile.id })

        // create new user if not found
        if (!user) {
            user = new User({
                vkId: profile.id,
                name: profile.displayName
            })

            await user.save()
        }

        if (!user.stripeId) {
            const customer = await stripe.customers.create({
                description: `Customer for ${user._id}`,
                email: user.username
            })
    
            user.stripeId = customer.id

            await user.save()
        }

        return done(null, user)
    } catch (e) {
        return done(e)
    }
}))

/**
 * twitter auth
 */
passport.use(new TwitterStrategy({
    consumerKey: env.twitter.clientId,
    consumerSecret: env.twitter.clientSecret,
    callbackURL: `${env.appUrl}/`
}, async (token, refreshToken, profile, done) => {
    try {
        let user = await User.findOne({ 'twitterId': profile.id })

        console.log(profile)

        // create new user if not found
        if (!user) {
            user = new User({
                twitterId: profile.id,
                name: profile.displayName
            })

            await user.save()
        }

        if (!user.stripeId) {
            const customer = await stripe.customers.create({
                description: `Customer for ${user._id}`,
                email: user.username
            })
    
            user.stripeId = customer.id

            await user.save()
        }

        return done(null, user)
    } catch (e) {
        return done(e)
    }
}))

passport.serializeUser((user, done) => {
    done(null, user.id)
})

passport.deserializeUser(async (userId, done) => {
    try {
        const user = await User.findById(userId)
        return done(null, user)
    } catch (e) {
        return done(e)
    }
})

export default passport
