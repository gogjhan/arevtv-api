import nodemailer from 'nodemailer'
import env from 'env'

const transporter = nodemailer.createTransport(env.nodemailer)

export default transporter
