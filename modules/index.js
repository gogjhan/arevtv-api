import util from 'util'
import fs from 'fs'

export async function assign (receiver, content, model) {
    try {
        console.log(content.body)
        Object.entries(content.body).forEach(([key, value]) => {
            if (!model.protectedFields.includes(key)) {
                if (model.translatable && model.translatable.includes(key)) {
                    let data
                    try {
                        data = JSON.parse(value)
                    } catch (e) {
                        data = value
                    }

                    if (data.en) receiver[key].en = data.en
                    if (data.ru) receiver[key].ru = data.ru
                    if (data.hy) receiver[key].hy = data.hy
                } else {
                    try {
                        receiver[key] = JSON.parse(value)
                    } catch (e) {
                        receiver[key] = value
                    }
                
                    if (model.schema.paths[key]) {
                        if (model.schema.paths[key].constructor.name === 'ObjectId') {
                            if (!value) {
                                receiver[key] = undefined
                            }
                        } else if (model.schema.paths[key].constructor.name === 'SchemaArray') {
                            if (!value.length) {
                                receiver[key] = undefined
                            }
                        }
                    }
                }
            }
        })

        let previousImages = []
        if (content.files) {
            Object.keys(content.files).forEach(key => {
                previousImages.push(receiver[key])
                receiver[key] = content.files[key][0].path
            })
        }
    
        await receiver.save()
        for (let image of previousImages) {
            unlink(image)
        }
    } catch (e) {
        console.log(e)
        return Promise.reject(e)
    }
}

export function unlink (path) {
    if (path && fs.existsSync(path)) {
        return util.promisify(fs.unlink)(path)
    }
    
    return Promise.resolve()
}

export function handleErrors (e) {
    try {
        console.log(JSON.stringify(e))
    } catch (err) {
        console.log(e)
    }
}