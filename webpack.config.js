const nodeExternals = require('webpack-node-externals')

module.exports = {
    devtool: 'eval-source-map', // controls how source maps are generated
    entry: [
        'babel-polyfill', // emulate a full ES2015+ environment, to be able to use new features like Promises, Array.from, Object.assign...
        './server.js'
    ],
    output: {
        path: __dirname,
        filename: 'build.js'
    },
    module: {
        rules: [
            // {
            //     enforce: 'pre', 
            //     test: /\.js$/, 
            //     exclude: /node_modules/, 
            //     loader: 'eslint-loader' 
            // },
            { 
                test: /\.js$/, 
                exclude: /node_modules/, 
                loader: 'babel-loader',
                options: {
                    retainLines: true
                }
            }
          ]
    },
    target: 'node', // in order to ignore built-in modules like path, fs, etc.
    externals: [nodeExternals()], // in order to ignore all modules in node_modules folder,
    resolve: { // tell where to look for files when called, i.e. if written import 'mongoose' where to look for mongoose package
        modules: [
            __dirname,
            'node_modules'
        ]
    }
}
