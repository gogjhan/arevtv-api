import { Router } from 'express'

import { 
    authController,
    paymentController
} from 'controllers'

const router = new Router()

router.get('/', authController.get)
router.put('/update', authController.update)

router.post('/payments/card', paymentController.addCard)
router.post('/payments/inecopay', paymentController.viaInecopay)
router.post('/payments/stripe', paymentController.viaStripe)
router.delete('/payments/stripe', paymentController.unsubscribeStripe)

export default router
