import { Router } from 'express'

import adminRoutes from './admin'
import userRoutes from './user'
import * as middleware from 'middleware'

import { 
    slideController,
    projectController,
    videoController,
    personController,
    pageController,
    adController,
    paymentController,
    settingsController
} from 'controllers'

const router = new Router()

router.use('/admin', [middleware.ensureAuthenticated, middleware.admin], adminRoutes)
router.use('/user', middleware.ensureAuthenticated, userRoutes)

router.get('/slides', slideController.all)

router.get('/videos', videoController.all)
router.get('/videos/:id', videoController.get)
router.get('/videos/:locale/:slug', videoController.getViaSlug)
router.get('/videos/:id/vote', middleware.ensureAuthenticated, videoController.vote)

router.get('/projects', projectController.all)
router.get('/projects/:id', projectController.get)
router.get('/projects/:locale/:slug', projectController.getViaSlug)

router.get('/people', personController.all)
router.get('/people/:id', personController.get)

router.get('/pages', pageController.all)
router.get('/pages/:slug', pageController.get)

router.get('/ads', adController.all)
router.get('/ads/:placement', adController.get)

router.post('/pay/idram', paymentController.viaIdram)

router.get('/settings', settingsController.all)

export default router
