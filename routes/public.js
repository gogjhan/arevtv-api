import { Router } from 'express'

import { paymentController } from 'controllers'

const router = new Router()

router.post('/paypal/callback', paymentController.viaPaypal)

export default router
