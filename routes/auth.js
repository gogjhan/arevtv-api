import { Router } from 'express'
import * as authController from 'controllers/authController'
import passport from 'modules/passport'

const router = new Router()

router.post('/register', authController.register)
router.post('/verify', authController.verify)
router.post('/password-recovery', authController.resetPassword)
router.put('/password-recovery', authController.newPassword)
router.post('/local', passport.authenticate('local'), authController.login)
router.get('/facebook', passport.authenticate('facebook'), authController.login)
router.get('/instagram', passport.authenticate('instagram'), authController.login)
router.get('/vkontakte', passport.authenticate('vkontakte'), authController.login)
router.get('/logout', authController.logout)

export default router
