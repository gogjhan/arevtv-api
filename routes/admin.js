import { Router } from 'express'
import upload from 'modules/uploader'

import { 
    userController,
    slideController,
    pageController,
    projectController,
    videoController,
    personController,
    adController,
    settingsController
} from 'controllers'

const router = new Router()

router.get('/users/seed-username', userController.seedUsername)

// users
router.get('/users', userController.all)
router.get('/users/:id', userController.get)
router.put('/users/:id', userController.update)
router.delete('/users/:id', userController.remove)

// slides
router.post('/slides', upload('/slides').fields([{ name: 'cover' }]), slideController.add)
router.put('/slides/:id', upload('/slides').fields([{ name: 'cover' }]), slideController.update)
router.delete('/slides/:id', slideController.remove)

// movies
router.post('/projects', upload('/projects').fields([
    { name: 'poster', maxCount: 1 },
    { name: 'cover', maxCount: 1 },
    { name: 'banner', maxCount: 1 }
]), projectController.add)
router.put('/projects/:id', upload('/projects').fields([
    { name: 'poster', maxCount: 1 },
    { name: 'cover', maxCount: 1 },
    { name: 'banner', maxCount: 1 }
]), projectController.update)
router.delete('/projects/:id', projectController.remove)

// videos
router.post('/videos', upload('/videos').fields([{ name: 'cover', maxCount: 1 }]), videoController.add)
router.put('/videos/:id', upload('/videos').fields([{ name: 'cover', maxCount: 1 }]), videoController.update)
router.delete('/videos/:id', videoController.remove)

// people
router.post('/people', upload('/people').fields([{ name: 'image' }]), personController.add)
router.put('/people/:id', upload('/people').fields([{ name: 'image' }]), personController.update)
router.delete('/people/:id', personController.remove)

// pages
router.post('/pages', upload('/pages').fields([{ name: 'image' }]), pageController.add)
router.put('/pages/:id', upload('/pages').fields([{ name: 'image' }]), pageController.update)
router.delete('/pages/:id', pageController.remove)

// ads
router.post('/ads', upload('/ads').fields([{ name: 'image' }]), adController.add)
router.put('/ads/:id', upload('/ads').fields([{ name: 'image' }]), adController.update)
router.delete('/ads/:id', adController.remove)

// settings
router.put('/settings', upload('/videos').fields([{ name: 'liveVideoCover', maxCount: 1 }]), settingsController.update)

export default router
