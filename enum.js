export class ERROR {
    static UNAUTHORIZED = 401
    static VALIDATION = 602
    static WRONG_CREDENTIALS = 604
    static INVALID_ACTION = 605
    static PERMISSION_DENIED = 606
    static INVALID_DATA = 610
}